package com.jpmc.challengecode

import com.jpmc.challengecode.domain.repository.SchoolRepository
import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.remote.dto.SchoolStatDto

class MockSchoolRepository(private val schoolsDto: List<SchoolDto>): SchoolRepository {

    override suspend fun getSchools(): List<SchoolDto> {
        return schoolsDto
    }

    override suspend fun getSchoolStatInfo(dbn: String): List<SchoolStatDto> {
        val schoolStatDto = SchoolStatDto(
            dbn = "MD",
            schoolName = "NYC Schools",
            avgWritingScore = "12355",
            avgReadingScore = "222",
            avgMathScore = "200",
            numOfSatTestTakers = "10"
        )
        return mutableListOf(schoolStatDto)
    }
}