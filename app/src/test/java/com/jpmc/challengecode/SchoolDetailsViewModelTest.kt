package com.jpmc.challengecode

import com.jpmc.challengecode.domain.use_cases.get_school_stat.GetSchoolStatInfoUseCase
import com.jpmc.challengecode.domain.use_cases.get_schools.GetSchoolsUseCase
import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.ui.school_list.SchoolListViewModel
import com.jpmc.challengecode.ui.school_stat.SchoolDetailViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolDetailsViewModelTest {
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: SchoolDetailViewModel
    private lateinit var getSchoolStatInfoUseCase: GetSchoolStatInfoUseCase
    private lateinit var mockSchoolRepository: MockSchoolRepository


    @Test
    fun test_get_schools_details_loading_success() {
        mockSchoolRepository = MockSchoolRepository(mutableListOf())
        getSchoolStatInfoUseCase = GetSchoolStatInfoUseCase(mockSchoolRepository)
        viewModel = SchoolDetailViewModel(getSchoolStatInfoUseCase)

        testCoroutineRule.runBlocking {
            viewModel.getSchoolDetails("MD")

            val iterate = viewModel.state.iterator()
            if (iterate.hasNext()) {
                val state = iterate.next()
                assert(state is SchoolDetailViewModel.State.Loading)
            }

            if (iterate.hasNext()) {
                val state = iterate.next()
                assert(state is SchoolDetailViewModel.State.Success)
            }
        }
    }
}