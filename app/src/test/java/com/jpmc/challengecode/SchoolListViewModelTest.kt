package com.jpmc.challengecode

import com.jpmc.challengecode.domain.use_cases.get_schools.GetSchoolsUseCase
import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.ui.school_list.SchoolListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolListViewModelTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: SchoolListViewModel
    private lateinit var getSchoolsUseCase: GetSchoolsUseCase
    private lateinit var mockSchoolRepository: MockSchoolRepository


    @Test
    fun test_get_schools_loading_success() {
        val schoolDto = SchoolDto(
            dbn = "MD",
            schoolName = "NYC Schools",
            phoneNumber = "12355",
            schoolEmail = "test@test.com",
            location = "Plano",
            website = "www.example.com",
            totalStudents = "100"
        )

        mockSchoolRepository = MockSchoolRepository(mutableListOf(schoolDto))
        getSchoolsUseCase = GetSchoolsUseCase(mockSchoolRepository)
        viewModel = SchoolListViewModel(getSchoolsUseCase)

        testCoroutineRule.runBlocking {
            viewModel.getSchools()

            val iterate = viewModel.state.iterator()
            if (iterate.hasNext()) {
                val state = iterate.next()
                assert(state is SchoolListViewModel.State.Loading)
            }

            if (iterate.hasNext()) {
                val state = iterate.next()
                assert(state is SchoolListViewModel.State.Success)
            }
        }
    }
}