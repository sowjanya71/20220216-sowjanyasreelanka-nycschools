package com.jpmc.challengecode

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jpmc.challengecode.common.Result
import com.jpmc.challengecode.ui.school_list.SchoolListScreen
import com.jpmc.challengecode.ui.school_list.SchoolListViewModel
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest

@RunWith(AndroidJUnit4::class)
class FragmentSchoolsListInstrumentedTest: AutoCloseKoinTest() {
    private lateinit var scenario: FragmentScenario<SchoolListScreen>
    private val schoolListViewModel: SchoolListViewModel = mockk(relaxed = true)

    @Before
    fun setUp() {
        startKoin {
            modules( module { schoolListViewModel })
        }
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_CodingChallenge)
        scenario.moveToState(newState = Lifecycle.State.STARTED)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun assert_schools_list() {
        //Facing below issue Caused by: java.io.IOException:
        // Unable to dlopen libmockkjvmtiagent.so: dlopen failed: library "libmockkjvmtiagent.so" not found
        // Due to time constraint cannot work on it.
    }
}