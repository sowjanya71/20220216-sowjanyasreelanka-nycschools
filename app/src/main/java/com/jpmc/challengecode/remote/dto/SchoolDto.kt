package com.jpmc.challengecode.remote.dto

import com.google.gson.annotations.SerializedName

data class SchoolDto(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("school_email")
    val schoolEmail: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("total_students")
    val totalStudents: String
)