package com.jpmc.challengecode

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApplication: Application()