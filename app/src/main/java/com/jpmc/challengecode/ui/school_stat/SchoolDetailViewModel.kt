package com.jpmc.challengecode.ui.school_stat

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.challengecode.common.Result
import com.jpmc.challengecode.domain.model.SchoolStatInfo
import com.jpmc.challengecode.domain.use_cases.get_school_stat.GetSchoolStatInfoUseCase
import javax.inject.Inject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val getSchoolStatInfoUseCase: GetSchoolStatInfoUseCase
) : ViewModel() {
    private var schoolDetails: List<SchoolStatInfo> = emptyList()
    private val _state = Channel<State>(Channel.RENDEZVOUS)
    val state: ReceiveChannel<State>
        get() = _state

    fun getSchoolDetails(dbn: String) {
        viewModelScope.launch {
            if (schoolDetails.isNotEmpty()) {
                _state.send(State.Success(schoolDetails))
            } else {
                getSchoolStatInfoUseCase(dbn).onEach { result ->
                    when (result) {
                        is Result.Success -> {
                            schoolDetails = result.data ?: emptyList()
                            _state.send(State.Success(schoolDetails))
                        }
                        is Result.Empty -> {
                            _state.send(State.Empty(result.message ?: ""))
                        }
                        is Result.Error -> {
                            _state.send(State.Error(result.message ?: ""))
                        }
                        is Result.Loading -> {
                            _state.send(State.Loading)
                        }
                    }
                }.launchIn(this)
            }
        }


    }

    sealed class State {
        object Loading : State()
        class Empty(val message: String) : State()
        class Error(val message: String) : State()
        class Success(val schoolDeatils: List<SchoolStatInfo>) : State()
    }
}