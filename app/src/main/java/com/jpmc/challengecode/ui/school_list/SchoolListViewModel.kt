package com.jpmc.challengecode.ui.school_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.challengecode.domain.model.Schools
import com.jpmc.challengecode.domain.use_cases.get_schools.GetSchoolsUseCase
import com.jpmc.challengecode.common.Result
import javax.inject.Inject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val getSchoolsUseCase: GetSchoolsUseCase
) : ViewModel() {
    private var schoolList: List<Schools> = emptyList()
    private val _state = Channel<State>(Channel.RENDEZVOUS)
    val state: ReceiveChannel<State>
        get() = _state

    fun getSchools() {
        viewModelScope.launch {
            if (schoolList.isNotEmpty()) {
                _state.send(State.Success(schoolList))
            } else {
                getSchoolsUseCase.invoke().onEach { result ->
                    when (result) {
                        is Result.Success -> {
                            schoolList = result.data ?: emptyList()
                            _state.send(State.Success(schoolList))
                        }
                        is Result.Empty -> {
                            _state.send(State.Empty(result.message ?: ""))
                        }
                        is Result.Error -> {
                            _state.send(State.Error(result.message ?: ""))
                        }
                        is Result.Loading -> {
                            _state.send(State.Loading)
                        }
                    }
                }.launchIn(this)
            }
        }


    }

    sealed class State {
        object Loading : State()
        class Empty(val message: String) : State()
        class Error(val message: String) : State()
        class Success(val schoolList: List<Schools>) : State()
    }
}