package com.jpmc.challengecode.ui.school_list.adapter

import androidx.recyclerview.widget.DiffUtil
import com.jpmc.challengecode.domain.model.Schools

class ListDiffUtil : DiffUtil.ItemCallback<Schools>() {
    override fun areItemsTheSame(oldItem: Schools, newItem: Schools): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Schools, newItem: Schools): Boolean {
        return oldItem.schoolName == newItem.schoolName
    }
}