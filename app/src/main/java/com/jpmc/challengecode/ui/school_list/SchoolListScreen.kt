package com.jpmc.challengecode.ui.school_list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jpmc.challengecode.R
import com.jpmc.challengecode.databinding.FragmentSchoolListScreenBinding
import com.jpmc.challengecode.domain.model.Schools
import com.jpmc.challengecode.ui.school_list.adapter.ListAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SchoolListScreen : Fragment() {

    private val viewModel: SchoolListViewModel by viewModels()
    private lateinit var binding: FragmentSchoolListScreenBinding

    private val schoolAdapter by lazy {
        ListAdapter(onItemClicked)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolListScreenBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeState()
        viewModel.getSchools()
    }

    private fun initViews() {
        binding.recyclerView.apply {
            adapter = schoolAdapter
            addItemDecoration(
                DividerItemDecoration(
                    requireActivity(),
                    LinearLayoutManager.VERTICAL
                )
            )
        }
    }

    private val onItemClicked = fun(item: Schools) {
        findNavController().navigate(
            R.id.action_schoolListScreen_to_schoolDetailScreen,
            Bundle().apply {
                putParcelable("schoolInfo", item)
            })
    }

    private fun observeState() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            for (state in viewModel.state) {
                when (state) {
                    is SchoolListViewModel.State.Success -> {
                        binding.progressbar.visibility = View.GONE
                        binding.error.visibility = View.GONE
                        schoolAdapter.submitList(state.schoolList.toMutableList())
                    }

                    is SchoolListViewModel.State.Error -> {
                        binding.progressbar.visibility = View.GONE
                        binding.error.visibility = View.VISIBLE
                    }

                    is SchoolListViewModel.State.Empty -> {
                        binding.progressbar.visibility = View.GONE
                        binding.error.visibility = View.VISIBLE
                        binding.error.setText(R.string.list_empty)
                    }

                    is SchoolListViewModel.State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                        binding.error.visibility = View.GONE
                    }
                }
            }
        }
    }
}