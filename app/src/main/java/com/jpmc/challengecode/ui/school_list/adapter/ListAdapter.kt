package com.jpmc.challengecode.ui.school_list.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.challengecode.databinding.SchoolListItemBinding
import com.jpmc.challengecode.domain.model.Schools

class ListAdapter(val onClick: (Schools) -> Unit) :
    ListAdapter<Schools, com.jpmc.challengecode.ui.school_list.adapter.ListAdapter.ViewHolder>(ListDiffUtil()) {

    private lateinit var binding: SchoolListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = SchoolListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { school ->
            holder.bind(school)
            holder.itemView.setOnClickListener {
                onClick(school)
            }
        }
    }

    class ViewHolder(private val binding: SchoolListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(school: Schools) {
            binding.schoolName.text = school.schoolName
        }
    }
}