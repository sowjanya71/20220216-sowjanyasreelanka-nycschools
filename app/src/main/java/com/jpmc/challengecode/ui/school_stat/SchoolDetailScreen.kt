package com.jpmc.challengecode.ui.school_stat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.jpmc.challengecode.databinding.SchoolDetailScreenBinding
import com.jpmc.challengecode.domain.model.SchoolStatInfo
import com.jpmc.challengecode.domain.model.Schools
import dagger.hilt.android.AndroidEntryPoint

/**
 * Class to display school details screen
 */
@AndroidEntryPoint
class SchoolDetailScreen : Fragment() {
    private val viewModel: SchoolDetailViewModel by viewModels()
    private lateinit var binding: SchoolDetailScreenBinding

    private val school: Schools by lazy {
        checkNotNull(requireArguments().getParcelable("schoolInfo"))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolDetailScreenBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeState()
        viewModel.getSchoolDetails(school.dbn)
    }


    /**
     * Initialize the view
     */
    private fun initView() {
        binding.topAppBar.apply {
            setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    /**
     * Observe the state of corountine scope which was launched in viewmodel
     * and update the UI based on state
     */
    private fun observeState() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            for (state in viewModel.state) {
                when (state) {
                    is SchoolDetailViewModel.State.Success -> {
                        binding.error.visibility = View.GONE
                        binding.progressbar.visibility = View.GONE
                        updateView(state.schoolDeatils[0], school)
                    }

                    is SchoolDetailViewModel.State.Error -> {
                        binding.error.visibility = View.VISIBLE
                        binding.progressbar.visibility = View.GONE
                        //Need to handle network is unavailable scneario, if network is not available then give an alert
                       // and navigate user Device Network setting.to enable it
                    }

                    is SchoolDetailViewModel.State.Empty -> {
                        binding.progressbar.visibility = View.GONE
                        binding.error.visibility = View.GONE
                        updateView(null, school)
                    }

                    is SchoolDetailViewModel.State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                        binding.error.visibility = View.GONE
                    }
                }
            }
        }
    }

    /**
     * Map the views text with required information fetched
     * from school and schoolstat object
     */
    private fun updateView(schoolDetail: SchoolStatInfo?, school: Schools) {
        binding.schoolName.text = school.schoolName
        binding.phoneNumber.text = school.phoneNumber
        binding.email.text = school.emailId ?: "-"
        binding.totalStudents.text = school.totalStudents
        binding.website.text = school.website
        binding.address.text = school.location.substringBefore("(")

        binding.math.text = schoolDetail?.avgMathScore ?: "-"
        binding.write.text = schoolDetail?.avgWritingScore ?: "-"
        binding.read.text = schoolDetail?.avgReadingScore ?: "-"
        binding.testTakers.text = schoolDetail?.numOfSatTestTakers ?: "-"
    }
}