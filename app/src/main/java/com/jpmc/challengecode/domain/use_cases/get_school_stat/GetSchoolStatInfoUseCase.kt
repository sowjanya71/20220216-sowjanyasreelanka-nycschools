package com.jpmc.challengecode.domain.use_cases.get_school_stat

import com.jpmc.challengecode.domain.repository.SchoolRepository
import com.jpmc.challengecode.common.Result
import com.jpmc.challengecode.domain.model.SchoolStatInfo
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow
import retrofit2.HttpException
import java.io.IOException

/**
 * To get school state information from remote repository and covert to domain object
 */
class GetSchoolStatInfoUseCase @Inject constructor(
    private val repository: SchoolRepository
) {
    operator fun invoke(dbn: String): Flow<Result<List<SchoolStatInfo>>> = flow {
        try {
            emit(Result.Loading())
            val schoolStatInfo = repository.getSchoolStatInfo(dbn).map { schoolStatDto ->
                SchoolStatInfo(
                    dbn = schoolStatDto.dbn,
                    schoolName = schoolStatDto.schoolName,
                    avgMathScore = schoolStatDto.avgMathScore,
                    avgReadingScore = schoolStatDto.avgReadingScore,
                    avgWritingScore = schoolStatDto.avgWritingScore,
                    numOfSatTestTakers = schoolStatDto.numOfSatTestTakers
                )
            }

            if (schoolStatInfo.isEmpty()) {
                emit(Result.Empty())
            } else {
                emit(Result.Success(schoolStatInfo))
            }

        } catch (e: HttpException) {
            emit(
                Result.Error(
                    e.localizedMessage ?: "An unexpected error occurred"
                )
            )
        } catch (e: IOException) {
            emit(
                Result.Error(
                    e.localizedMessage ?: "An unexpected error occurred"
                )
            )
        }
    }
}