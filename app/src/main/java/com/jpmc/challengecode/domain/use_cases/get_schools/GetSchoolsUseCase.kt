package com.jpmc.challengecode.domain.use_cases.get_schools

import com.jpmc.challengecode.domain.model.Schools
import com.jpmc.challengecode.domain.repository.SchoolRepository
import com.jpmc.challengecode.common.Result
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow
import retrofit2.HttpException
import java.io.IOException

/**
 * To get schools list from remote repository and covert to domain objects
 */
class GetSchoolsUseCase @Inject constructor(
    private val repository: SchoolRepository
) {
    operator fun invoke(): Flow<Result<List<Schools>>> = flow {
        try {
            emit(Result.Loading())
            val schools = repository.getSchools().map {
                Schools(
                    dbn = it.dbn,
                    schoolName = it.schoolName,
                    phoneNumber = it.phoneNumber,
                    emailId = it.schoolEmail,
                    website = it.website,
                    totalStudents = it.totalStudents,
                    location = it.location
                )
            }.sortedBy { it.schoolName }

            if (schools.isEmpty()) {
                emit(Result.Empty())
            } else {
                emit(Result.Success(schools))
            }

        } catch (e: HttpException) {
            emit(
                Result.Error(
                    e.localizedMessage ?: "An unexpected error occurred"
                )
            )
        } catch (e: IOException) {
            emit(
                Result.Error(
                    e.localizedMessage ?: "An unexpected error occurred"
                )
            )
        }
    }
}