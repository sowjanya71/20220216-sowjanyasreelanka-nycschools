package com.jpmc.challengecode.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Schools(
    val dbn: String,
    val schoolName: String,
    val phoneNumber: String,
    val emailId: String?,
    val location: String,
    val website: String,
    val totalStudents: String
): Parcelable