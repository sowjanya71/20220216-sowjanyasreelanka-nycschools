package com.jpmc.challengecode.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolStatInfo(
    val dbn: String,
    val schoolName: String,
    val numOfSatTestTakers: String,
    val avgReadingScore: String,
    val avgMathScore: String,
    val avgWritingScore: String
): Parcelable