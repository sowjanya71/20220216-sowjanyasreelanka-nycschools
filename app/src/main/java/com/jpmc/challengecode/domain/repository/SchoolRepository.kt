package com.jpmc.challengecode.domain.repository

import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.remote.dto.SchoolStatDto

/**
 * Repository for network calls
 */
interface SchoolRepository {
    suspend fun getSchools(): List<SchoolDto>
    suspend fun getSchoolStatInfo(dbn: String): List<SchoolStatDto>

}