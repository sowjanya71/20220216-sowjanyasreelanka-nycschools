package com.jpmc.challengecode.repository

import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.remote.dto.SchoolStatDto
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchools(): List<SchoolDto>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSchoolStatInfo(@Query("dbn") dbn: String): List<SchoolStatDto>
}