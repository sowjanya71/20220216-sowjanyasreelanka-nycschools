package com.jpmc.challengecode.repository

import com.jpmc.challengecode.domain.repository.SchoolRepository
import com.jpmc.challengecode.remote.dto.SchoolDto
import com.jpmc.challengecode.remote.dto.SchoolStatDto
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val api: SchoolApi
): SchoolRepository {
    override suspend fun getSchools(): List<SchoolDto> {
        return api.getSchools()
    }

    override suspend fun getSchoolStatInfo(dbn: String): List<SchoolStatDto> {
        return api.getSchoolStatInfo(dbn)
    }
}