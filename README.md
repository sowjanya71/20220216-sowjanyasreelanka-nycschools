# NYC Schools

Libraries Used:
   * Retrofit
   * OkHttp
   * Jetpack navigation
   * Hilt for dependency Injection
   * Kotlin Coroutines for Async networking

Architechture:
   * Followed Clean architecture which is a slight improvement over MVVM design
   * Helps in scaling the features with proper organisation of code
   * Used Hilt for dependency Injection
   * Used View binding instead of the required data binding. Data binding comes with its own overhead like more compile time and difficult in debugging.

Improvements:
  * Test cases are not completed due to time contraints
  * With clean architechture, all the business logics are moved to use cases. We can test each use case by creating fake dependencies.
  * Espresso for UI unit and integration testing
